﻿using api.Entity;
using Microsoft.EntityFrameworkCore;

namespace RevenuesAPI.Config
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
        }

        public DbSet<Revenue> Revenues { get; set; }
    }
}
