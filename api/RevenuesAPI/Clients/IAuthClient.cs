﻿using Refit;
using RevenuesAPI.Clients.Requests;
using RevenuesAPI.Clients.Responses;

namespace RevenuesAPI.Clients
{
    public interface IAuthClient
    {
        [Post("/api/Login")]
        Task<LoginRes> LoginAsync([Body] CredentialsReq credentials);
    }
}
