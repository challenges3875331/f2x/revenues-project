﻿namespace RevenuesAPI.Clients.Requests
{
    public class CredentialsReq
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public CredentialsReq(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}
