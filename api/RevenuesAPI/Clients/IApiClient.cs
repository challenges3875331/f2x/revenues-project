﻿using Refit;
using RevenuesAPI.Clients.Responses;

namespace RevenuesAPI.Clients
{
    public interface IApiClient
    {
        [Get("/api/RecaudoVehiculos/{date}")]
        Task<List<RevenueVehicleRes>> GetRevenueVehicles(string date);

        [Get("/api/ConteoVehiculos/{date}")]
        Task<List<QuantityVehicleRes>> GetQuantityVehicles(string date);
    }
}
