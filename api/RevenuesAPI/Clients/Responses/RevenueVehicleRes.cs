﻿using System.Text.Json.Serialization;

namespace RevenuesAPI.Clients.Responses
{
    public class RevenueVehicleRes
    {
        [JsonPropertyName("estacion")]
        public string Station { get; set; }

        [JsonPropertyName("sentido")]
        public string Direction { get; set; }

        [JsonPropertyName("hora")]
        public int Hour { get; set; }

        [JsonPropertyName("categoria")]
        public string Category { get; set; }

        [JsonPropertyName("valorTabulado")]
        public int Value { get; set; }
    }
}
