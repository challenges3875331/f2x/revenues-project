﻿namespace RevenuesAPI.Clients.Responses
{
    public class LoginRes
    {
        public string Token { get; set; }
        public string Expiration { get; set; }
    }
}
