﻿using System.Text.Json.Serialization;

namespace RevenuesAPI.Clients.Responses
{

    public class QuantityVehicleRes
    {
        [JsonPropertyName("estacion")]
        public string Station { get; set; }

        [JsonPropertyName("sentido")]
        public string Direction { get; set; }

        [JsonPropertyName("hora")]
        public int Hour { get; set; }

        [JsonPropertyName("categoria")]
        public string Category { get; set; }

        [JsonPropertyName("cantidad")]
        public int Quantity { get; set; }
    }

}
