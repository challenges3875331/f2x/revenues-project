using Microsoft.AspNetCore.Mvc;
using RevenuesAPI.Dtos;
using RevenuesAPI.Services;

namespace RevenuesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/revenues")]
    public class RevenuesController : ControllerBase
    {
        private readonly ILogger<LoadDataController> _logger;
        private readonly RevenueService _revenueService;
        private readonly MonthlyReportService _monthlyReportService;

        public RevenuesController(ILogger<LoadDataController> logger, RevenueService revenueService,
            MonthlyReportService monthlyReportService)
        {
            _logger = logger;
            _revenueService = revenueService;
            _monthlyReportService = monthlyReportService;
        }

        [HttpPost(Name = "GetAll")]
        public async Task<IActionResult> Get([FromBody] RevenueReq req)
        {
            var result = _revenueService.GetAllRevenues(req);
            return await Task.FromResult<IActionResult>(Ok(result));
        }

        [HttpPost("report")]
        public async Task<IActionResult> GetReport([FromBody] MonthlyReportReq req)
        {
            var result = _monthlyReportService.GetMonthlyReport(req);
            return await Task.FromResult<IActionResult>(Ok(result));
        }
        
        [HttpPost("report/generate")]
        public IActionResult GenerateReport([FromBody] MonthlyReportReq req)
        {
            var result = _monthlyReportService.GetMonthlyReport(req);
            var excelStream = ExcelReportService.GenerateExcelFile(result, "report.xlsx");
            
            var response = new FileStreamResult(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    FileDownloadName = "report-xd.xlsx"
                };
            excelStream.Position = 0;
            return response;
        }
    }
}