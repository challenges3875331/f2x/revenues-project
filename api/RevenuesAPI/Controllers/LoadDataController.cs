using Microsoft.AspNetCore.Mvc;
using RevenuesAPI.Dtos;
using RevenuesAPI.Exceptions;
using RevenuesAPI.Services;

namespace RevenuesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/revenues")]
    public class LoadDataController : ControllerBase
    {

        private readonly ILogger<LoadDataController> _logger;
        private readonly LoadDataService _loadDataUseCase;

        public LoadDataController(ILogger<LoadDataController> logger, LoadDataService loadDataUseCase
            )
        {
            _logger = logger;
            _loadDataUseCase = loadDataUseCase;
        }

        [HttpPost("load-data")]
        public async Task<IActionResult> Get([FromBody] LoadDataReq requestData)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequest("Error in input fields");
            }
            return await Task.FromResult<IActionResult>(Ok(await _loadDataUseCase.ProcessData(requestData)));
        }
    }
}