﻿using System.Net.Http.Headers;
using Refit;
using RevenuesAPI.Clients;
using RevenuesAPI.Clients.Responses;

namespace RevenuesAPI.Services
{
    public class ApiService
    {

        private readonly ILogger _logger;
        private readonly IApiClient _apiService;

        public ApiService(string apiUrl, string accessToken)
        {
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole(); 
            });

            _logger = loggerFactory.CreateLogger<ApiService>();

            
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(apiUrl)
            };

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            _apiService = RestService.For<IApiClient>(httpClient);
        }


        public async Task<List<QuantityVehicleRes>> GetQuantityVehicles(string formattedDate)
        {
            try
            {
                var response = await _apiService.GetQuantityVehicles(formattedDate);
                _logger.LogInformation(" Items found for quantity: {count}", response.Count);
                return response;
            }
            catch (ApiException ex)
            {
                _logger.LogInformation(" Error consuming service revenues: {statusCode}", ex.StatusCode);
                return new List<QuantityVehicleRes>();
            }
        }

        public async Task<List<RevenueVehicleRes>> GetRevenueVehicles(string formattedDate)
        {
            try
            {
                var response = await _apiService.GetRevenueVehicles(formattedDate);
                _logger.LogInformation(" Items found for revenues: {count}", response.Count);
                return response;
            }
            catch (ApiException ex)
            {
                _logger.LogInformation(" Error consuming service revenues: {status}", ex.StatusCode);
                return new List<RevenueVehicleRes>();
            }
        }
    }
}
