﻿using RevenuesAPI.Clients.Requests;
using RevenuesAPI.Clients.Responses;
using RevenuesAPI.Dtos;
using RevenuesAPI.Repositories;

namespace RevenuesAPI.Services
{
    public class LoadDataService
    {
        private ApiService? _apiService;
        private readonly ILogger<LoadDataService> _logger;
        private readonly CommandRevenueRepository _commandRevenueRepository;


        public LoadDataService(ILogger<LoadDataService> logger, CommandRevenueRepository commandRevenueRepository)
        {
            _logger = logger;
            _commandRevenueRepository = commandRevenueRepository;
        }
        
        public async Task<object> ProcessData(LoadDataReq requestData)
        {
            var apiUrl = Environment.GetEnvironmentVariable("apiUrl")!;

            var authService = new AuthService(apiUrl);
            var accessToken = await authService.GetAccessTokenAsync(new CredentialsReq(
                Environment.GetEnvironmentVariable("username")!, 
                Environment.GetEnvironmentVariable("password")!));
            
            _apiService = new ApiService(apiUrl, accessToken);
            
            for (var date = requestData.InitialDate; date <= requestData.EndDate; date = date.AddDays(1))
            {
                await ProcessDataByDate(date);
            }

            return new { message = "Load data completed" };
        }

        private async Task ProcessDataByDate(DateOnly date)
        {
            var formattedDate = date.ToString("yyyy-MM-dd");
            _logger.LogInformation("Consuming apis for {formattedDate}", formattedDate);

            var revenueVehicleRes = await _apiService!.GetRevenueVehicles(formattedDate);
            var quantityVehicleRes = await _apiService!.GetQuantityVehicles(formattedDate);

            var mergedList = MergeLists(revenueVehicleRes, quantityVehicleRes, date);
            if (mergedList.Count > 0)
            {
                _commandRevenueRepository.DeleteByDate(new DateTime(date.Year, date.Month, date.Day));
                _commandRevenueRepository.SaveAll(mergedList);
            }

            _logger.LogInformation("Finished consuming apis for {formattedDate} with {count} records", formattedDate, mergedList.Count);
        }

        private List<RevenueDto> MergeLists(IReadOnlyCollection<RevenueVehicleRes> revenueList,
            IReadOnlyCollection<QuantityVehicleRes> quantityList, DateOnly date)
        {
            var unifiedList = revenueList
                .Select(revenue => new RevenueDto
                {
                    Station = revenue.Station,
                    Direction = revenue.Direction,
                    Hour = revenue.Hour,
                    Date = date,
                    Category = revenue.Category,
                    Value = revenue.Value,
                    Quantity = quantityList
                        .Where(quantity =>
                            quantity.Station == revenue.Station &&
                            quantity.Direction == revenue.Direction &&
                            quantity.Hour == revenue.Hour &&
                            quantity.Category == revenue.Category)
                        .Select(quantity => quantity.Quantity)
                        .FirstOrDefault()
                })
                .ToList();

            var unmatchedQuantityItems = quantityList
                .Where(quantity =>
                    !revenueList.Any(revenue =>
                        revenue.Station == quantity.Station &&
                        revenue.Direction == quantity.Direction &&
                        revenue.Hour == quantity.Hour &&
                        revenue.Category == quantity.Category))
                .Select(quantity => new RevenueDto
                {
                    Station = quantity.Station,
                    Direction = quantity.Direction,
                    Hour = quantity.Hour,
                    Date = date,
                    Category = quantity.Category,
                    Quantity = quantity.Quantity,
                    Value = 0
                });

            unifiedList.AddRange(unmatchedQuantityItems);

            return unifiedList;
        }
    }
}