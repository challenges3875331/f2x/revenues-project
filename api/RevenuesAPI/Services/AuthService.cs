﻿using Refit;
using RevenuesAPI.Clients;
using RevenuesAPI.Clients.Requests;

namespace RevenuesAPI.Services
{
    public class AuthService
    {

        private readonly IAuthClient _authClient;

        public AuthService(string apiUrl)
        {
            _authClient = RestService.For<IAuthClient>(apiUrl);
        }


        public async Task<string> GetAccessTokenAsync(CredentialsReq credentialsReq)
        {
            try
            {
                var response = await _authClient.LoginAsync(credentialsReq);
                return response.Token;
            }
            catch (ApiException ex)
            {
                throw;
            }
        }
    }
}
