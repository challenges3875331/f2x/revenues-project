﻿using RevenuesAPI.Dtos;
using RevenuesAPI.Exceptions;
using RevenuesAPI.Repositories;

namespace RevenuesAPI.Services
{
    
    
    public class MonthlyReportService
    {

        private readonly GetMonthlyReportRepository _getMonthlyReportRepository;

        public MonthlyReportService(GetMonthlyReportRepository getMonthlyReportRepository)
        {
            _getMonthlyReportRepository = getMonthlyReportRepository;
        }

        public MonthlyReportProcessDto GetMonthlyReport(MonthlyReportReq monthlyReportReq)
        {
            var infoReport = _getMonthlyReportRepository.GetRevenueTotalsByMonthAndStations(
                monthlyReportReq.Month, monthlyReportReq.Year, new string[0]);
            var infoReportList = infoReport.ToList();

            if (infoReportList.Count == 0)
            {
                throw new ResourceNotFound("Not revenues found for this month");
            }
            
            var daysInMonth = Enumerable.Range(1, DateTime.DaysInMonth(monthlyReportReq.Year, monthlyReportReq.Month))
                .Select(day => new DateOnly(monthlyReportReq.Year, monthlyReportReq.Month, day))
                .ToList();
            
            var uniqueStations = infoReportList
                .Select(entry => entry.Station)
                .Distinct()
                .ToList();
            
            return ProcessDataReport(daysInMonth, infoReportList, uniqueStations);
        }

        private static MonthlyReportProcessDto ProcessDataReport(List<DateOnly> daysInMonth,
            List<MonthlyReportDto> infoReportList, List<string> uniqueStations)
        {
            var map = daysInMonth.ToDictionary(
                date => date,
                date => infoReportList
                    .Where(entry => entry.Date == date)
                    .GroupBy(entry => entry.Station)
                    .ToDictionary(
                        group => group.Key,
                        group => new ReportValues(TotalQuantity: group.Sum(entry => entry.TotalQuantity),
                            TotalValue: group.Sum(entry => entry.TotalValue))
                    )
            );

            return new MonthlyReportProcessDto()
            {
                UniqueStations = uniqueStations,
                Map = map
            };
        }
    }
}
