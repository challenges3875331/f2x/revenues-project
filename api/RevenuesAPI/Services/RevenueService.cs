﻿using RevenuesAPI.Dtos;
using RevenuesAPI.Repositories;

namespace RevenuesAPI.Services
{
    public class RevenueService
    {

        private readonly GetRevenuesRepository _getRevenuesRepository;

        public RevenueService(GetRevenuesRepository getRevenuesRepository)
        {
            _getRevenuesRepository = getRevenuesRepository;
        }

        public PageDto<RevenueDto> GetAllRevenues(RevenueReq req)
        {
            return _getRevenuesRepository.GetAllRevenues(req);
        }

    }
}
