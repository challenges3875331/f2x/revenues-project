using OfficeOpenXml;
using OfficeOpenXml.Style;
using RevenuesAPI.Dtos;

namespace RevenuesAPI.Services;

public class ExcelReportService
{
    public static MemoryStream GenerateExcelFile(MonthlyReportProcessDto monthlyReportProcess, string filePath)
    {
        var file = new FileInfo(filePath);
        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        using var package = new ExcelPackage(file);
        var worksheet = package.Workbook.Worksheets.Add("Reporte Mensual");

        FillHeaderRow(worksheet, monthlyReportProcess);
        FillDataRows(worksheet, monthlyReportProcess);
        FillTotalByStationRow(worksheet, monthlyReportProcess);
        FillTotalValueAndQuantity(worksheet, monthlyReportProcess);
        worksheet.Cells.AutoFitColumns();
        
        FixWidthColumns(worksheet);
        
        var memoryStream = new MemoryStream();
        
        memoryStream.Seek(0, SeekOrigin.Begin);
        package.SaveAs(memoryStream);
        
        return memoryStream;
    }

    private static void FillHeaderRow(ExcelWorksheet worksheet, MonthlyReportProcessDto monthlyReportProcess)
    {
        const int startColumn = 2;
        const int startRow = 2;

        var titleHeader = worksheet.Cells[startRow, startColumn, startRow + 1, startColumn];
        titleHeader.AutoFitColumns();
        SetHeaderStyles(titleHeader, "");

        Enumerable.Range(0, monthlyReportProcess.UniqueStations.Count).ToList().ForEach(i =>
        {
            var startCol = startColumn + 1 + (i * 2);

            var stationRange = worksheet.Cells[startRow, startCol, startRow, startCol + 1];
            SetHeaderStyles(stationRange, monthlyReportProcess.UniqueStations[i]);

            var valueHeaderCell = worksheet.Cells[startRow + 1, startCol];
            SetHeaderStyles(valueHeaderCell, "Valor");

            var quantityHeaderCell = worksheet.Cells[startRow + 1, startCol + 1];
            SetHeaderStyles(quantityHeaderCell, "Cantidad");
        });
    }
    private static void FillDataRows(ExcelWorksheet worksheet, MonthlyReportProcessDto monthlyReportProcess)
    {
        const int startColumn = 2;
        const int startRow = 2;

        var dataStartRow = startRow + 2;
        var dataStartCol = startColumn + 1;

        var rowIndex = dataStartRow;

        foreach (var (date, stationValues) in monthlyReportProcess.Map)
        {
            var dateCell = worksheet.Cells[rowIndex, startColumn];
            SetHeaderStyles(dateCell, date.ToString());

            var colIndex = 0;
            foreach (var station in monthlyReportProcess.UniqueStations)
            {
                var values = stationValues.GetValueOrDefault(station, new ReportValues(0, 0));
                
                var valueCell = worksheet.Cells[rowIndex, dataStartCol + colIndex * 2];
                valueCell.Style.Numberformat.Format = "\\$ #,##0";
                SetBodyStyles(valueCell, values.TotalValue);

                var quantityCell = worksheet.Cells[rowIndex, dataStartCol + colIndex * 2 + 1];
                SetBodyStyles(quantityCell, values.TotalQuantity);

                colIndex++;
            }

            rowIndex++;
        }
    }

    private static void FillTotalByStationRow(ExcelWorksheet worksheet, MonthlyReportProcessDto monthlyReportProcess)
    {
        const int startColumn = 2;
        const int startRow = 2;

        var totalStartRow = monthlyReportProcess.Map.Count + 2 + startRow;

        var totalHeaderCell = worksheet.Cells[totalStartRow, startColumn];
        SetHeaderStyles(totalHeaderCell, "Total");

        var dataStartCol = startColumn + 1;
        for (var j = 0; j < monthlyReportProcess.UniqueStations.Count; j++)
        {
            var startCol = dataStartCol + j * 2;

            var totalValueCell = worksheet.Cells[totalStartRow, startCol];
            totalValueCell.Style.Numberformat.Format = "\\$ #,##0";
            SetFormulaStyles(totalValueCell,
                $"SUM({worksheet.Cells[dataStartCol + 1, startCol].Address}:{worksheet.Cells[totalStartRow - 1, startCol].Address})");

            var totalQuantityCell = worksheet.Cells[totalStartRow, startCol + 1];
            SetFormulaStyles(totalQuantityCell,
                $"SUM({worksheet.Cells[dataStartCol + 1, startCol + 1].Address}:{worksheet.Cells[totalStartRow - 1, startCol + 1].Address})");
        }
    }
    
    private static void FillTotalValueAndQuantity(ExcelWorksheet worksheet, MonthlyReportProcessDto monthlyReportProcess)
    {
        const int startColumn = 2;
        const int startRow = 2;

        var totalStartRow = monthlyReportProcess.Map.Count + 2 + startRow;

        var totalValueHeaderCell = worksheet.Cells[totalStartRow + 2, startColumn];
        SetHeaderStyles(totalValueHeaderCell, "Total Valor");
        
        var totalQuantityHeaderCell = worksheet.Cells[totalStartRow + 3, startColumn];
        SetHeaderStyles(totalQuantityHeaderCell, "Total Cantidad");
        
        var dataStartCol = startColumn + 1;
        var formulaValue = worksheet.Cells[totalStartRow, dataStartCol].Address;
        var formulaQuantity = worksheet.Cells[totalStartRow, dataStartCol + 1].Address;
        
        for (var j = 1; j < monthlyReportProcess.UniqueStations.Count; j++)
        {
            var startCol = dataStartCol + j * 2;

            formulaValue += " + " + worksheet.Cells[totalStartRow, startCol].Address;
            formulaQuantity += " + " + worksheet.Cells[totalStartRow, startCol + 1].Address;
        }
        
        var totalValueCell = worksheet.Cells[totalStartRow + 2, startColumn + 1];
        totalValueCell.Style.Numberformat.Format = "\\$ #,##0";
        SetFormulaStyles(totalValueCell, formulaValue);

        var totalQuantityCell = worksheet.Cells[totalStartRow + 3, startColumn + 1];
        SetFormulaStyles(totalQuantityCell, formulaQuantity);
        
    }
    
    private static void FixWidthColumns(ExcelWorksheet worksheet)
    {
        for (var col = 1; col <= worksheet.Dimension.End.Column; col++)
        {
            worksheet.Column(col).Width = worksheet.Column(col).Width + 5;
        }
    }
    
    private static void SetBorders(ExcelRange range, ExcelBorderStyle borderStyle)
    {
        range.Style.Border.Top.Style = borderStyle;
        range.Style.Border.Bottom.Style = borderStyle;
        range.Style.Border.Left.Style = borderStyle;
        range.Style.Border.Right.Style = borderStyle;
    }

    private static void SetHeaderStyles(ExcelRange range, string name)
    {
        range.Merge = true;
        range.Value = name;
        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        range.Style.Font.Bold = true;
        SetBorders(range, ExcelBorderStyle.Thin);
    }

    private static void SetBodyStyles(ExcelRange range, long number)
    {
        range.Value = number;
        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        SetBorders(range, ExcelBorderStyle.Thin);
    }

    private static void SetFormulaStyles(ExcelRange range, string formula)
    {
        range.Formula = formula;
        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        range.Style.Font.Bold = true;
        SetBorders(range, ExcelBorderStyle.Thin);
    }
    
}