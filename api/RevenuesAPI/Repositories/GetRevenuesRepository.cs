﻿using api.Entity;
using Microsoft.EntityFrameworkCore;
using RevenuesAPI.Config;
using RevenuesAPI.Dtos;
using RevenuesAPI.Repositories.Filters;
using static System.DateOnly;

namespace RevenuesAPI.Repositories
{

    public class GetRevenuesRepository
    {

        private readonly ApplicationDbContext _context;
        private readonly Dictionary<string, string> _entityFieldMap;
        private readonly RevenueFilterStrategy _revenueFilterStrategy;

        public GetRevenuesRepository(ApplicationDbContext context)
        {
            _context = context;
            _entityFieldMap = InitSortFieldMap();
            _revenueFilterStrategy = new RevenueFilterStrategy();
        }

        private Dictionary<string, string> InitSortFieldMap()
        {
            return new Dictionary<string, string>
            {
                { "STATION", "Station" },
                { "DIRECTION", "Direction" },
                { "CATEGORY", "Category" },
                { "HOUR", "Hour" },
                { "DATE", "Date" },
                { "VALUE", "Value" },
                { "QUANTITY", "Quantity" },
            };
        }

        public PageDto<RevenueDto> GetAllRevenues(RevenueReq req)
        {
            var query = _context.Revenues.AsQueryable();
            query = ApplyFilters(query, req.Filters);

            var totalElements = query.Count();

            query = ApplySort(query, req.Sort);
            query = ApplyPagination(query, req.Page);

           var data = query.Select(r => new RevenueDto
            {
                Station = r.Station,
                Direction = r.Direction,
                Hour = r.Hour,
                Category = r.Category,
                Quantity = r.Quantity,
                Value = r.Value,
                Date = FromDateTime(r.Date)
            }).ToList();

            return new PageDto<RevenueDto>
            {
                TotalElements = totalElements,
                PageNumber = req.Page.PageNumber,
                PageSize = req.Page.PageSize,
                Data = data
            };
        }

        private IQueryable<Revenue> ApplyPagination(IQueryable<Revenue> query, Page page)
        {
            var skip = (page.PageNumber - 1) * page.PageSize;
            query = query.Skip(skip).Take(page.PageSize);
            return query;
        }

        private IQueryable<Revenue> ApplySort(IQueryable<Revenue> query, Sort sort)
        {
            var fieldEntity = _entityFieldMap.GetValueOrDefault(sort.Field.ToUpper(), "Date");
            return sort.Order.ToUpper() == "ASC"
                ? query.OrderByDescending(r => EF.Property<object>(r, fieldEntity))
                : (IQueryable<Revenue>)query.OrderBy(r => EF.Property<object>(r, fieldEntity));
        }

        private IQueryable<Revenue> ApplyFilters(IQueryable<Revenue> query, IEnumerable<Filter> filters)
        {
            filters
                .Where(filter => filter.IsOk()).ToList()
                .ForEach(filter => query = ValidateAndApplyFilter(query, filter));
            return query;
        }

        private IQueryable<Revenue> ValidateAndApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            var fieldEntity = _entityFieldMap!.GetValueOrDefault(filter.Field.ToUpper(), null);
            if (fieldEntity == null) return query;
            
            filter.Field = fieldEntity;
            if (_revenueFilterStrategy.FilterStrategies.TryGetValue(filter.Field, out var filterStrategy))
            {
                query = filterStrategy.ApplyFilter(query, filter);
            }

            return query;
        }

    }
}
