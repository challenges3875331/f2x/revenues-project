﻿using api.Entity;
using RevenuesAPI.Config;
using RevenuesAPI.Dtos;

namespace RevenuesAPI.Repositories
{

    public class CommandRevenueRepository
    {

        private readonly ApplicationDbContext _context;

        public CommandRevenueRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void SaveAll(List<RevenueDto> revenueDtoList)
        {
            var revenueList = revenueDtoList
                .Select(revenueDto => new Revenue
                {
                    Station = revenueDto.Station,
                    Direction = revenueDto.Direction,
                    Hour = revenueDto.Hour,
                    Date = revenueDto.Date.ToDateTime(TimeOnly.MinValue),
                    Category = revenueDto.Category,
                    Quantity = revenueDto.Quantity,
                    Value = revenueDto.Value
                })
                .ToList();

            _context.Revenues.AddRange(revenueList);
            _context.SaveChanges();
        }
        
        public void DeleteByDate(DateTime dateToDelete)
        {
            var revenuesToDelete = _context.Revenues
                .Where(revenue => revenue.Date.Date == dateToDelete.Date)
                .ToList();

            _context.Revenues.RemoveRange(revenuesToDelete);
            _context.SaveChanges();
        }

    }
}
