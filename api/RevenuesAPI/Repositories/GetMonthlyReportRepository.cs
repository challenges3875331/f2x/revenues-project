﻿using RevenuesAPI.Config;
using RevenuesAPI.Dtos;

namespace RevenuesAPI.Repositories
{

    public class GetMonthlyReportRepository
    {

        private readonly ApplicationDbContext _context;

        public GetMonthlyReportRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<MonthlyReportDto> GetRevenueTotalsByMonthAndStations(int month, int year, string[] stationNames)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            var query = _context.Revenues
                .Where(r => r.Date >= startDate && r.Date <= endDate);

            if (stationNames.Length > 0)
            {
                query = query.Where(r => stationNames.Contains(r.Station));
            }

            var result = query
                .GroupBy(r => new { r.Date, r.Station })
                .Select(g => new MonthlyReportDto
                {
                    Date = DateOnly.FromDateTime(g.Key.Date),
                    Station = g.Key.Station,
                    TotalQuantity = g.Sum(r => r.Quantity),
                    TotalValue = g.Sum(r => r.Value)
                })
                .ToList();

            return result;
        }

    }
}
