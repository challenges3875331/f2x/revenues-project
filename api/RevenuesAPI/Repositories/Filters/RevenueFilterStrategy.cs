﻿using api.Entity;
using RevenuesAPI.Dtos;
using RevenuesAPI.Utils;

namespace RevenuesAPI.Repositories.Filters
{

    public class RevenueFilterStrategy
    {
        public Dictionary<string, IFilterStrategy<Revenue>> FilterStrategies { get; } = new()
        {
            { "Station", new StationFilterStrategy() },
            { "Direction", new DirectionFilterStrategy() },
            { "Category", new CategoryFilterStrategy() },
            { "Date", new DateFilterStrategy() },
            { "Hour", new HourFilterStrategy() },
            { "Value", new ValueFilterStrategy() },
            { "Quantity", new QuantityFilterStrategy() },
        };
    }

    public class StationFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            return query.Where(r => r.Station.Contains(filter.Value));
        }
    }

    public class CategoryFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            return query.Where(r => r.Category.Equals(filter.Value));
        }
    }

    public class DirectionFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            return query.Where(r => r.Direction.Contains(filter.Value));
        }
    }

    public class DateFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            var dateValue = ConverterUtil.ConvertToDateTimeOrDefault(filter.Value);
            return dateValue == null ? query : query.Where(r => r.Date.Equals(dateValue));
        }
    }

    public class HourFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            var intValue = ConverterUtil.ConvertToIntOrDefault(filter.Value);
            return intValue == null ? query : query.Where(r => r.Hour.Equals(intValue));
        }
    }

    public class ValueFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            var intValue = ConverterUtil.ConvertToLongOrDefault(filter.Value);
            return intValue == null ? query : query.Where(r => r.Value.Equals(intValue));
        }

    }

    public class QuantityFilterStrategy : IFilterStrategy<Revenue>
    {
        public IQueryable<Revenue> ApplyFilter(IQueryable<Revenue> query, Filter filter)
        {
            var intValue = ConverterUtil.ConvertToIntOrDefault(filter.Value);
            return intValue == null ? query : query.Where(r => r.Quantity.Equals(intValue));
        }
    }
}
