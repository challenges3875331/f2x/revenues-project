﻿using RevenuesAPI.Dtos;

namespace RevenuesAPI.Repositories.Filters
{
    public interface IFilterStrategy<TEntity>
    {
        IQueryable<TEntity> ApplyFilter(IQueryable<TEntity> query, Filter filter);
    }
}
