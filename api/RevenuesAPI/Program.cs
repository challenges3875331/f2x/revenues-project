using dotenv.net;
using RevenuesAPI.Middlewares;
using Microsoft.EntityFrameworkCore;
using RevenuesAPI.Config;
using RevenuesAPI.Repositories;
using RevenuesAPI.Services;


var builder = WebApplication.CreateBuilder(args);
DotEnv.Load();

// Add the repository and service classes.
builder.Services.AddScoped<GetRevenuesRepository>();
builder.Services.AddScoped<GetMonthlyReportRepository>();
builder.Services.AddScoped<CommandRevenueRepository>();

builder.Services.AddScoped<RevenueService>();
builder.Services.AddScoped<LoadDataService>();
builder.Services.AddScoped<MonthlyReportService>();

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(Environment.GetEnvironmentVariable("connectionString")));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseCors(policyBuilder =>
    policyBuilder
        .WithOrigins(Environment.GetEnvironmentVariable("webClientUrl")!)
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors();

app.UseMiddleware<ExceptionMiddleware>();
app.UseMiddleware<AuthorizationMiddleware>();

app.MapGet("/api/v1/health", () => "Welcome to API Revenues");

app.UseAuthorization();

app.MapControllers();

app.Run();