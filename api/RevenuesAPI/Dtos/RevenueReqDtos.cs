﻿using System.ComponentModel.DataAnnotations;
using Microsoft.IdentityModel.Tokens;

namespace RevenuesAPI.Dtos
{
    public class RevenueReq
    {
        public List<Filter> Filters { get; set; }
        public Sort Sort { get; set; }
        public Page Page { get; set; }

        public RevenueReq()
        {
            Filters = new List<Filter>();
            Sort = new Sort();
            Page = new Page();
        }
    }

    public class Filter
    {
        public string Field { get; set; }
        public string Value { get; set; }

        public bool IsOk()
        {
            return !Field.IsNullOrEmpty() && !Value.IsNullOrEmpty();
        }

    }

    public class Sort
    {
        public string Field { get; set; }
        public string Order { get; set; }

        public Sort()
        {
            Field = "";
            Order = "";
        }
    }

    public class Page
    {
        [Range(1, int.MaxValue, ErrorMessage = "PageNumber must be greater than or equal to 1.")]
        public int PageNumber { get; set; }
        [Range(10, int.MaxValue, ErrorMessage = "PageSize must be greater than or equal to 10.")]
        public int PageSize { get; set; }

        public Page()
        {
            PageNumber = 1;
            PageSize = 10;
        }
    }

}
