﻿namespace RevenuesAPI.Dtos
{

    public class PageDto<T>
    {
        public int TotalElements { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalPages => (int) Math.Ceiling((double)TotalElements / PageSize);
        public List<T> Data { get; set; } = new List<T>();
    }

    public class RevenueDto
    {
        public string Station { get; set; }
        public string Direction { get; set; }
        public int Hour { get; set; }
        public string Category { get; set; }
        public int Quantity { get; set; }
        public long Value { get; set; }
        public DateOnly Date { get; set; }
    }
}
