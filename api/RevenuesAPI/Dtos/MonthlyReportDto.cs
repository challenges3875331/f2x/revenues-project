﻿namespace RevenuesAPI.Dtos
{

    public record ReportValues(long TotalQuantity, long TotalValue);
    
    public class MonthlyReportDto
    {
        public string Station { get; set; }
        public int TotalQuantity { get; set; }
        public long TotalValue { get; set; }
        public DateOnly Date { get; set; }
    }
    
    public class MonthlyReportProcessDto
    {
        public Dictionary<DateOnly, Dictionary<string, ReportValues>> Map { get; set; }
        public List<string> UniqueStations { get; set; }
    }
    
}
