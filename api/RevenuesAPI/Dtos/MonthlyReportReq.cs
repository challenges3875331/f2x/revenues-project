﻿using System.ComponentModel.DataAnnotations;

namespace RevenuesAPI.Dtos
{
    public class MonthlyReportReq
    {
        [Range(1900, 9999, ErrorMessage = "Year is invalid")]
        [Required(ErrorMessage = "Year is required.")]
        public int Year { get; set; }
        [Range(1, 12, ErrorMessage = "Month is invalid")]
        [Required(ErrorMessage = "Month is required.")]
        public int Month { get; set; }
    }
}