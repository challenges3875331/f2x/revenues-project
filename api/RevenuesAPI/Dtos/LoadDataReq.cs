﻿using System.ComponentModel.DataAnnotations;

namespace RevenuesAPI.Dtos
{
    public class LoadDataReq
    {
        [Required(ErrorMessage = "Initial date is required.")]
        public DateOnly InitialDate { get; set; }
        [Required(ErrorMessage = "Final date is required.")]
        public DateOnly EndDate { get; set; }
    }
}