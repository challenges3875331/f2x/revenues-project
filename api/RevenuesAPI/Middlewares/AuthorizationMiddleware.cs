using RevenuesAPI.Exceptions;
using RevenuesAPI.Utils;

namespace RevenuesAPI.Middlewares;

public class AuthorizationMiddleware
{
    private readonly RequestDelegate _next;
    private const string UnauthorizedMessageException = "Acceso no autorizado";

    public AuthorizationMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context)
    {
        if (ShouldValidateCookie(context))
        {
            var bearerToken = context.Request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(bearerToken))
            {
                var token = bearerToken.ToString().Split(' ')[1];
                if (JwtUtil.ValidateToken(token))
                {
                    await _next(context);
                }
                else
                {
                    throw new UnauthorizedException(UnauthorizedMessageException);
                }
            }
            else
            {
                throw new UnauthorizedException(UnauthorizedMessageException);
            }
        }
        else
        {
            await _next(context);
        }
    }
    
    private static bool ShouldValidateCookie(HttpContext context)
    {
        if (!context.Request.Path.StartsWithSegments("/api/v1/health")) return true;
        return context.Request.Method != HttpMethods.Get;
    }
}