using System.Net;
using System.Text.Json;
using RevenuesAPI.Exceptions;

namespace RevenuesAPI.Middlewares;

public class ExceptionMiddleware
{
    private readonly ILogger<ExceptionMiddleware> _logger;
    private readonly RequestDelegate _next;
    private const string UnexpectedError = "Ha ocurrido un error inesperado";

    public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (AggregateException ex)
        {
            var message = GetInnermostExceptionMessage(ex);

            var statusCode = ex.InnerExceptions.Aggregate(HttpStatusCode.InternalServerError, (current, innerEx) =>
                innerEx.InnerException switch
                {
                    BadRequest _ => HttpStatusCode.BadRequest,
                    ResourceNotFound _ => HttpStatusCode.NotFound,
                    _ => current
                });
            _logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            await context.Response.WriteAsync(GetJsonResponse(context.Response.StatusCode, 
                ((int)statusCode == 500)? UnexpectedError : message));
        }
        catch (BadRequest ex)
        {
            _logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            await context.Response.WriteAsync(GetJsonResponse(context.Response.StatusCode, ex.Message));
        }
        catch (ResourceNotFound ex)
        {
            _logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            await context.Response.WriteAsync(GetJsonResponse(context.Response.StatusCode, ex.Message));
        }
        catch (UnauthorizedException ex)
        {
            _logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            await context.Response.WriteAsync(GetJsonResponse(context.Response.StatusCode, ex.Message));
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await context.Response.WriteAsync(GetJsonResponse(context.Response.StatusCode,
                UnexpectedError));
        }
    }

    private static string GetJsonResponse(int errorCode, string message)
    {
        var options = new JsonSerializerOptions()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        };
        return JsonSerializer.Serialize(new ExceptionResponse(errorCode, message), options);
    }

    private static string GetInnermostExceptionMessage(Exception ex)
    {
        while (ex.InnerException != null) ex = ex.InnerException;
        return ex.Message;
    }
}