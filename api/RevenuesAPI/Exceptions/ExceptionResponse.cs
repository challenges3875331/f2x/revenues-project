namespace RevenuesAPI.Exceptions;

public class ExceptionResponse
{
    public ExceptionResponse(int errorCode, string message)
    {
        ErrorCode = errorCode;
        Message = message;
        Timestamp = DateTime.Now;
    }

    public int ErrorCode { get; set; }
    public DateTime Timestamp { get; set; }
    public string Message { get; set; }
}