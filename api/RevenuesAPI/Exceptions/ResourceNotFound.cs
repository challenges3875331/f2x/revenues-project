namespace RevenuesAPI.Exceptions;

public class ResourceNotFound : Exception
{
    public ResourceNotFound(string? message) : base(message)
    {
    }
}