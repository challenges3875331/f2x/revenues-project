﻿using System.ComponentModel.DataAnnotations;

namespace api.Entity
{
    public class Revenue
    {
        [Key] public Guid Id { get; set; }

        [Required] [StringLength(255)] public string Station { get; set; }

        [Required] [StringLength(10)] public string Direction { get; set; }

        public int Hour { get; set; }

        [Required] [StringLength(2)] public string Category { get; set; }

        public int Quantity { get; set; }

        public long Value { get; set; }

        public DateTime Date { get; set; }
    }
}