﻿namespace RevenuesAPI.Utils
{
    public class ConverterUtil
    {

        public static int? ConvertToIntOrDefault(string input, int? defaultValue = null)
        {
            return int.TryParse(input, out int result) ? result : defaultValue;
        }

        public static long? ConvertToLongOrDefault(string input, long? defaultValue = null)
        {
            return long.TryParse(input, out long result) ? result : defaultValue;
        }

        public static DateTime? ConvertToDateTimeOrDefault(string input, DateTime? defaultValue = null)
        {
            return DateTime.TryParse(input, out DateTime result) ? result : defaultValue;
        }

    }
}
