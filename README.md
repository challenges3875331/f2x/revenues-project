# Aplicación Fullstack con Angular, .NET y Keycloak

Esta guía te proporcionará instrucciones detalladas para ejecutar la solución de la prueba técnica, un back-end .NET Core 6 y la opción de integración con Keycloak para la autenticación y autorización. Puedes ejecutar la aplicación tanto en Docker como localmente.

## Requisitos Previos

Asegúrate de tener instalados los siguientes componentes antes de comenzar:

- [Docker](https://www.docker.com/get-started)
- [Node.js](https://nodejs.org/)
- [.NET Core 6](https://dotnet.microsoft.com/download/dotnet/6.0)

## Configuración

1. Clona este repositorio en tu máquina local.

```bash
git clone <URL_DEL_REPOSITORIO>
```

2. Accede a la carpeta del repositorio clonado.

```bash
cd <NOMBRE_DEL_REPOSITORIO>
```

## Configuración del Back-end (.NET)

El back-end está ubicado en la carpeta `api`.

1. Si deseas utilizar Docker, primero revise el archivo `api/RevenuesAPI/docker-compose.yml` y asegurate de actualizar las credenciales. Luego ejecuta:

```bash
cd api
docker-compose up --build
```

2. Si no deseas utilizar Docker y ejecutarlo local, crea un archivo `.env` en la carpeta `api/RevenuesAPI` y configura las siguientes variables de entorno con tus propias credenciales:

```plaintext
connectionString=Server=<server>;Database=<database>;User=<user>;Password=<pwd>;Encrypt=false;
apiUrl=http://54.209.174.241:5200
issuer=http://34.125.152.223/keycloak
username=user
password=password
webClientUrl=http://localhost:4210
publicKey=MIIBIjANBgkqhkiG9w0BAQ...
```

3. Ejecutar el back-end directamente con los siguientes comandos:

```bash
cd api
dotnet restore
dotnet run
```

## Configuración del Front-end (Angular)

El front-end se encuentra en la carpeta `web`.

1. Si deseas usar Docker para el front-end:

```bash
cd web
docker-compose up --build
```

2. Si prefieres ejecutar el front-end localmente:

```bash
cd web
npm install
npm start
```

## Configuración de Keycloak (Opcional)

Si deseas utilizar Keycloak para autenticación y autorización, sigue estos pasos:

1. Accede a la carpeta `keycloak`.

```bash
cd keycloak
```

2. Ejecuta Keycloak con Docker:

```bash
docker-compose up -d
```

3. Ajusta la configuración del back-end:

   - En `api/RevenuesApi/.env`, actualiza el valor de `publicKey` con la clave pública de tu instancia de Keycloak.

4. Ajusta la configuración del front-end:

   - En `web/src/environments/environment.ts`, actualiza `keyCloakHostUrl` con la URL de tu instancia de Keycloak.

## Acceso a la Aplicación

Una vez que hayas configurado todo, podrás acceder a la aplicación en tu navegador:

- Front-end Angular: Docker: http://localhost:4210, Local: http://localhost:4200
- Back-end .NET: http://localhost:5091

O acceder a la aplicación desplegada en: http://34.125.152.223 con el usuario user_test y clave 123456

##  Tecnologías y Módulos
La aplicación está construida utilizando diversas tecnologías como Angular, .Net Core 6, KeyCloak, SqlServer y Docker y se divide en tres módulos principales:

**Carga de Datos desde API Externa a SQL Server**: Este módulo utiliza tecnologías como .NET Core 6 para desarrollar la lógica de negocio y el acceso a la base de datos SQL Server. Se comunica con una API externa para obtener datos y luego los almacena en la base de datos. A pesar de que son dos endpoints distintos estos se guardan en una única tabla.

**Visualizador de Recaudos y Cantidad de Vehículos**: Este módulo utiliza Angular y primeng para construir la interfaz de usuario en el lado del cliente. Se conecta al back-end para obtener datos sobre recaudación y cantidad de vehículos, y proporciona funcionalidades como filtrado, ordenamiento y paginación para que los usuarios puedan interactuar con los datos.

**Generación de Reportes Mensuales en Formato Excel**: Este módulo está implementado en .NET Core. Su función es recopilar datos de la base de datos y generar reportes mensuales en formato Excel.

## Descripción de la Aplicación

El siguiente Diagrama arquitectura muestra una representación visual de la arquitectura general de la solución:

![Diagrama arquitectura](/imgs/diagram.png)

Flujo de autenticación/autorización implementado:

![Flujo Auth](/imgs/auth-flow.png)

Diagrama de base de datos propuesto:

![MER](/imgs/db-revenues.png)

La siguiente imagen muestra una captura de pantalla de la interfaz de usuario de la aplicación en ejecución. 

![Web UI Solución](/imgs/web-ui.jpg)

Ejemplo de formato excel de reporte mensual:

![Reporte excel](/imgs/xls-report.jpg)