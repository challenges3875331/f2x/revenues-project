export const environment = {
  production: true,
  envName: 'pdn',
  keyCloakHostUrl: 'http://34.125.152.223/keycloak',
  apiUrl: 'http://localhost:5091/api/v1/revenues',
  realm: 'revenues',
  clientId: 'revenues-app',
};
