import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RevenuesRoutingModule} from './revenues-routing.module';
import {ReportsComponent} from './components/reports/reports.component';
import {MainComponent} from './components/main/main.component';
import {ViewComponent} from './components/view/view.component';
import {LoadComponent} from './components/load/load.component';
import {ButtonModule} from "primeng/button";
import {SharedModule} from "../../shared/shared.module";
import {TableModule} from "primeng/table";
import {CardModule} from "primeng/card";
import {CalendarModule} from "primeng/calendar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MultiSelectModule} from "primeng/multiselect";
import {DropdownModule} from "primeng/dropdown";
import {InputNumberModule} from "primeng/inputnumber";
import {InputTextModule} from "primeng/inputtext";
import {ToastModule} from "primeng/toast";


@NgModule({
  declarations: [
    ReportsComponent,
    MainComponent,
    ViewComponent,
    LoadComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RevenuesRoutingModule,
    ButtonModule,
    TableModule,
    CardModule,
    CalendarModule,
    ReactiveFormsModule,
    MultiSelectModule,
    DropdownModule,
    FormsModule,
    InputNumberModule,
    ToastModule,
    InputTextModule
  ]
})
export class RevenuesModule {
}
