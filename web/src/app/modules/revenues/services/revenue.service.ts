import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Revenue} from "../models/revenue";
import {RevenueReq} from "../models/revenue.req";
import {PageDto} from "../models/page";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RevenueService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getRevenues(req: RevenueReq): Observable<PageDto<Revenue>> {
    return this.http.post<PageDto<Revenue>>(`${this.apiUrl}`, req);
  }

  loadData(req: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/load-data`, req);
  }

  generateMonthlyReport(req: any): Observable<Blob> {
    return this.http.post<Blob>(`${this.apiUrl}/report/generate`, req, {
      responseType: 'blob' as 'json'
    });
  }

}
