import {Component, OnInit} from '@angular/core';
import {RevenueService} from "../../services/revenue.service";
import {RevenueReq} from "../../models/revenue.req";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  revenues!: any[];

  totalRecords!: number;

  loading: boolean = false;
  statuses: string[] = ["I", "II", "III", "IV", "V"];

  constructor(private revenueService: RevenueService) {
  }

  ngOnInit() {
    this.loading = true;
  }

  loadRevenues(event: any) {
    this.loading = true;

    this.revenueService.getRevenues(new RevenueReq(event)).subscribe({
      next: (res) => {
        this.revenues = res.data;
        this.totalRecords = res.totalElements;
        this.loading = false;
      },
      error: (err) => {
        console.error(err);
        this.loading = false;
      }
    });
  }

}
