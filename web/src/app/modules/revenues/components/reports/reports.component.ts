import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RevenueService} from "../../services/revenue.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  providers: [MessageService]
})
export class ReportsComponent implements OnInit {

  searchForm!: FormGroup;
  loading: boolean = false;

  constructor(private fb: FormBuilder, private revenueService: RevenueService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      month: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
      year: ['', [Validators.required, Validators.min(1900)]]
    });
  }

  private get monthField() {
    return this.searchForm.get('month')!;
  }

  private get yearField() {
    return this.searchForm.get('year')!;
  }

  onSubmit() {
    if (this.searchForm.valid) {
      this.loading = true;
      this.revenueService.generateMonthlyReport({
        "year": this.yearField.value,
        "month": this.monthField.value
      }).subscribe({
        next: (res: Blob) => {
          this.downloadFile(res);
          this.clearForm();
          this.loading = false;
          this.messageService.add({ severity: 'success', summary: 'Éxito', detail: 'Archivo generado exitosamente.' });
        },
        error: (err) => {
          if (err.status == 404) {
            this.messageService.add({ severity: 'info', summary: 'Info', detail: 'No se encontraron datos para el mes seleccionado.' });
          } else{
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se pudo generar el archivo.' });
          }
          this.loading = false;
        }
      });
    }
  }

  isMonthRequiredError(): boolean {
    return this.monthField.hasError('required') && this.monthField.touched;
  }

  isMonthRangeError(): boolean {
    return (this.monthField.hasError('min') || this.monthField.hasError('max')) && this.monthField.touched;
  }

  isYearRequiredError(): boolean {
    return this.yearField.hasError('required') && this.yearField.touched;
  }

  isYearRangeError(): boolean {
    return this.yearField.hasError('min') && this.yearField.touched;
  }

  private downloadFile(blob: Blob) {
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'reporte_mensual.xlsx';
    link.click();
    window.URL.revokeObjectURL(url);
  }

  private clearForm() {
    this.searchForm.reset();
  }

}
