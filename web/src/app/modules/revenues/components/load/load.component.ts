import {Component, OnInit} from '@angular/core';
import {MessageService} from "primeng/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RevenueService} from "../../services/revenue.service";
import {convertDate} from "../../../../shared/helpers/utilities";

@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.scss'],
  providers: [MessageService]
})
export class LoadComponent implements OnInit {
  searchForm!: FormGroup;
  loading: boolean = false;

  constructor(private fb: FormBuilder, private revenueService: RevenueService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.searchForm = this.fb.group({
      startDate: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
      endDate: ['', [Validators.required, Validators.min(1900)]]
    });
  }

  private get startDateField() {
    return this.searchForm.get('startDate')!;
  }

  private get endDateField() {
    return this.searchForm.get('endDate')!;
  }

  onSubmit() {
    if (this.searchForm.valid) {
      this.loading = true;
      this.revenueService.loadData({
        "initialDate":  convertDate(this.startDateField.value),
        "endDate":  convertDate(this.endDateField.value)
      }).subscribe({
        next: (res: any) => {
          this.clearForm();
          this.loading = false;
          this.messageService.add({ severity: 'success', summary: 'Éxito', detail: 'Data cargada correctamente.' });
        },
        error: (err) => {
          this.loading = false;
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se pudo cargar la data.' });
          console.error(err);
        }
      });
    }
  }

  isStartDateRequiredError(): boolean {
    return this.startDateField.hasError('required') && this.startDateField.touched;
  }

  isStartDateRangeError(): boolean {
    return !this.isValidDateRange() && this.startDateField.touched && this.endDateField.touched;
  }

  isEndDateRequiredError(): boolean {
    return this.endDateField.hasError('required') && this.endDateField.touched;
  }

  private isValidDateRange() {
    return this.startDateField.value <= this.endDateField.value;
  }

  private clearForm() {
    this.searchForm.reset();
  }

}
