import {Component} from '@angular/core';
import {KeycloakService} from "keycloak-angular";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  private selectedComponent = "";

  constructor(private readonly keycloak: KeycloakService) {
  }

  closeComponent() {
    this.selectedComponent = "";
  }

  selectComponent(name: string) {
    this.selectedComponent = name;
  }

  isSelectComponent(name: string) {
    return this.selectedComponent == name;
  }

  isSelectAnyComponent() {
    return this.selectedComponent != '';
  }

  onLogout() {
    this.keycloak.logout().then();
  }

}
