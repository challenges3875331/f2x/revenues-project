import {convertDate} from "../../../shared/helpers/utilities";

export class RevenueReq {
  public filters: Filter[];
  public sort: Sort;
  public page: Page;


  constructor(event: any) {
    this.page = new Page(event.first, event.rows);
    this.sort = new Sort(event.sortField, event.sortOrder);
    this.filters = this.convertToFilterArray(event.filters);
  }

  private convertToFilterArray(filters: any): Filter[] {
    return Object.entries(filters).reduce((acc: Filter[], [field, filter]: [string, any]) => {
      if (filter.value !== null) {
        acc.push(new Filter(field, filter.value, filter.matchMode));
      }
      return acc;
    }, []);
  }

}

export class Filter {
  private field: string;
  private value: string;
  private mode: string;

  constructor(field: string, value: any, mode: string) {
    this.field = field;
    this.value = value instanceof Date ? convertDate(value) : value.toString();
    this.mode = mode;
  }
}

export class Sort {
  public field: string;
  public order: string;

  constructor(field: string, order: number) {
    this.field = field ? field : "";
    this.order = order == -1 ? "DESC" : "ASC";
  }
}

export class Page {
  public pageNumber: number;
  public pageSize: number;

  constructor(page: number, pageSize: number) {
    this.pageNumber = (page / pageSize) + 1;
    this.pageSize = pageSize;
  }
}
