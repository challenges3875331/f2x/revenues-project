export interface PageDto<T> {
  totalElements: number;
  pageNumber: string;
  pageSize: string;
  totalPages: number;
  data: Array<T>;
}
