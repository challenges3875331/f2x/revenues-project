export interface Revenue {
  id: number;
  station: string;
  direction: string;
  hour: number;
  category: string;
  quantity: number;
  value: number;
  date: Date;
}
