import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-menu-card',
  templateUrl: './menu-card.component.html',
  styleUrls: ['./menu-card.component.scss']
})
export class MenuCardComponent {
  @Input() title!: string;
  @Input() description!: string;
  @Input() buttonLabel!: string;
  @Input() icon!: string;

  @Output() clickButton: EventEmitter<void> = new EventEmitter<void>();

  onClickButton(): void {
    this.clickButton.emit();
  }

}
