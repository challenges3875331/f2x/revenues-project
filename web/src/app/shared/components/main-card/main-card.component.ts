import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-main-card',
  templateUrl: './main-card.component.html',
  styleUrls: ['./main-card.component.scss']
})
export class MainCardComponent {
  @Input() title!: string;

  @Output() clickCloseButton: EventEmitter<void> = new EventEmitter<void>();

  onClickCloseButton(): void {
    this.clickCloseButton.emit();
  }

}
