import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuCardComponent} from "./components/menu-card/menu-card.component";
import {ButtonModule} from "primeng/button";
import {MainCardComponent} from './components/main-card/main-card.component';
import {CardModule} from "primeng/card";

@NgModule({
  declarations: [
    MenuCardComponent,
    MainCardComponent
  ],
  exports: [
    MenuCardComponent,
    MainCardComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    CardModule
  ]
})
export class SharedModule {
}
