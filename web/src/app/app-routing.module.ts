import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from "./modules/revenues/components/main/main.component";
import {AuthGuard} from "./shared/guards/auth.guard";

const routes: Routes = [
  {path: '', redirectTo: '/revenues', pathMatch: 'full'},
  {path: 'revenues', component: MainComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
